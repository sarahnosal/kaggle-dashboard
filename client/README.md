# Getting Started with Kaggle Dashboard!

This repo is an example Kaggle Dashboard. I created it to house three different visualizations showing the potential correlations between Card Type, Card Provider, and Distance between IP address and shipping address and Fraud Rate. 

### `Frontend`
To begin building my front-end I used `create-react-app` to start. First I began creating the underlying UI structure of my dashboard and my Redux store. My three visualizations are built using the Chart.JS library. I reccomend allowing some extra time to configure the options and settings for your charts because it can take a bit to get used to how they all work and read the documentation. Since your front end is not connected to a backend yet, you can use the Faker npm package to help generate some fake data quickly, so that way you can make sure your charts are set up right. 

### `Backend`
The backend is a basic FastAPI server and will only be concerned with retrieving data from the database. It is currently organized and setup to model how the projects you will be working on most likely will be set up and structured. The first step is to setup a simple HTTP GET endpoint and return a status 200 code plus a JSON array of your data. The CSV of data is huge, so you can use your best judgement here as far as limiting your data, or splitting it up into different network calls. If you're not sending a lot of data you can always stick to one endpoint and break up the JSON into your necessary parts for your visualizations, that's what I've done here. 

### `.env Files`
Since we are using a FastAPI server, we do not need to define the location of the backend in the proxy config (which is what the kaggle notebook reccomends). Instead we will be creating a .env file for both your front end and backend with the REACT_APP_API_BASE_ADDRESS defined in the client .env. I typically always use "http://localhost:8000/api/v1" because that's how the projects I work on are setup. You can change this though. The .env file in the server will contain the following settings:
- `POSTGRES_USER`
- `POSTGRES_PASSWORD`
- `POSTGRES_DB`
- `POSTGRES_SERVER`
- `POSTGRES_PORT`
- `SQLALCHEMY_PG_DATABASE_URI`
- `CORS_SETTINGS`
- `REQUEST_BASE_ADDRESS`

Many of these settings are confidential, so cannot be included in your gitlab repository. Feel free to reach out to your mentor for help getting them defined correctly if it takes to long to successfully connect to the database. 

### `Database`

You can deploy your own database and populate it with data from the train_transaction.csv that is provided with the kaggle training. The docker compose at the root level of the directory is already written to spin up the postgres Docker image allowing you to connect to the database and populate it with data. 


To get started run the following commands: 

npm i
npm start

Optional run: npx update-browserslist-db@latest

uvicorn app:app --reload


Tips: 

You have to run docker-compose to connect to the database in pgadmin, this is included in the start script