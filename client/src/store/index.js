import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import { dataSlice } from "./data.slice";

const reducer = combineReducers({
    data: dataSlice.reducer
})

const store = configureStore({
    reducer
    // middleware: [...getDefaultMiddleware({ serializableCheck: false })],

});

export default store