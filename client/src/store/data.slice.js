import { createSlice, createAsyncThunk, createReducer } from "@reduxjs/toolkit"
import { API } from "../config";

export const getData = createAsyncThunk(
    "data/getData",
    async (_, { getState, rejectWithValue }) => {
        const { user } = getState();
        console.log(user)
        // const { token } = user;
        try {
          const requestOptions = {
            method: "GET",
            headers: {
                Accept: "application.json", 
                "Content-Type": "application/json"
            },
          };
          const response = await fetch(`${API}/`, requestOptions);
    
          const data = await response.json();
          if (response.status === 200) {
            const copyData = { ...data };
            return copyData;
          }
          return rejectWithValue(data.detail);
        } catch (e) {
          return rejectWithValue(`${e}: An error has occurred within the server`);
        }
      }
);

export const aggregateData = createAsyncThunk(
  "data/aggregateData",
  async (_, { getState, rejectWithValue }) => {
      const { user } = getState();
      console.log(user)
      // const { token } = user;
      try {
        const requestOptions = {
          method: "GET",
          headers: {
              Accept: "application.json", 
              "Content-Type": "application/json"
          },
        };
        const response = await fetch(`${API}/aggregate-data`, requestOptions);
  
        const data = await response.json();
        if (response.status === 200) {
          const copyData = { ...data };
          return copyData;
        }
        return rejectWithValue(data.detail);
      } catch (e) {
        return rejectWithValue(`${e}: An error has occurred within the server`);
      }
    }
);

export const dataSlice = createSlice({
    name: "data",
    initialState: {
        isError: false,
        errorMessage: "",
        data: {},
        aggregatedData: {},
        isFetching: false,
    },
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getData.fulfilled, (state, { payload }) => {
            state.data = payload;
            state.isFetching = false;
            state.isError = false;
            return state;
        })
        builder.addCase(getData.pending, (state) => {
            state.isFetching = true;
        })
        builder.addCase(getData.rejected, (state, { payload }) => {
            state.isFetching = false;
            state.isError = true;
            state.errorMessage = payload;
        })  
        builder.addCase(aggregateData.fulfilled, (state, { payload }) => {
          state.aggregatedData = payload;
          state.isFetching = false;
          state.isError = false;
          return state;
      })
      builder.addCase(aggregateData.pending, (state) => {
          state.isFetching = true;
      })
      builder.addCase(aggregateData.rejected, (state, { payload }) => {
          state.isFetching = false;
          state.isError = true;
          state.errorMessage = payload;
      })  
    }
})

export const dataSelector = (state) => state.data