import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { GridContainer, Grid } from "@trussworks/react-uswds";
import { aggregateData, dataSelector, getData } from "../store/data.slice";
import BarChart1 from "../components/BarChart1";
import BarChart2 from "../components/BarChart2";
import SelectionPanel from "../components/SelectionPanel";
import LineChart from "../components/LineChart";
import { Loader } from "../components/Loader";

const Home = () => {
  const dispatch = useDispatch()
  const { aggregatedData } = useSelector(dataSelector)

  const [clicked, setClicked] =useState(true)
  const [display, setDisplay] = useState('Card Provider')


  useEffect(() => {
    console.log('Button clicked was:', display)
  },[display, clicked])

  useEffect(() => {
    dispatch(aggregateData())
  }, [dispatch])

  console.log(aggregatedData)

  return (
    <GridContainer containerSize="full" className='margin-3 ' >
      {aggregatedData.TransactionAmt ? (
        <Grid row gap style={{ height: "60vh"}} className='container'>
          <Grid col={2} style={{'margin': '25px', }}>
            <SelectionPanel 
              clicked={clicked} 
              setClicked={setClicked} 
              display={display}
              setDisplay={setDisplay}/>
          </Grid>
        <Grid col={9} style={{'margin': '25px', }}>
          {display === 'Card Provider' ? (
            <BarChart1 
              title='Fraud Rate vs. Card Provider'
              data={aggregatedData.card4}
            />) : null }
          {display === 'Card Type' ? (
            <BarChart2 
            title='Fraud Rate vs. Card Type'
            data={aggregatedData.card6}
            />) : null }
          {display === 'Distance' ? (
            <LineChart
            title='Fraud Rate vs. Distance' 
            dData={aggregatedData.dist1}/>
          ) : null }
        </Grid>
      </Grid>
      ) : (
        <Grid row>
          <Grid
            className="height-auto minh-tablet width-full display-flex flex-justify-center"
            col
          >
            <Loader />
          </Grid>
        </Grid>
      )}
    </GridContainer>
  )
}

export default Home