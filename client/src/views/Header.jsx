import React from "react";

const Header = () => {

  return (
    <div className='header'>
      <h1 id='title'>Fraud Dashboard</h1>
    </div>
  )
}

export default Header