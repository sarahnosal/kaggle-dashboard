import React from 'react'
import {
  BrowserRouter as Router, Route
} from 'react-router-dom'
import Home from './views/Home';
import Header from './views/Header';
import './index.css';
import "@trussworks/react-uswds/lib/uswds.css";
// import "@trussworks/react-uswds/lib/index.css";

function App() {
  return (
    <Router >
      <Header />
      <Route path="/" exact component={Home}/>
    </Router>
  );
}

export default App;
