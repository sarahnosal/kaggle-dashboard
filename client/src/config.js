export const API = process.env.REACT_APP_API_BASE_ADDRESS;
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  ENDPOINT: API,
};