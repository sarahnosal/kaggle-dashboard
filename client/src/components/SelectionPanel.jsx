import React, { useState } from 'react'
import { GridContainer, Grid, Button } from "@trussworks/react-uswds";

const SelectionPanel = ({clicked, setClicked, display, setDisplay}) => {

  const [chartButton, setChartButton] = useState('Provider')

  const chartButtons = [
    {id: 1, value:'Card Provider'},
    {id: 2, value:'Card Type'},
    {id: 3, value:'Distance'}
  ]

    return (
        <GridContainer style={{backgroundColor: "#D9D9D9", fontFamily: "'DM Sans', sans-serif", paddingLeft: '22px', paddingBottom: '26px', paddingTop: '15px'}}>
          <div style={{
            fontFamily: "'DM Sans', sans-serif", 
            fontWeight: 'bold', 
            color: "#2E2C2F", 
            paddingTop: '38px', 
            paddingLeft: '42px', 
            marginBottom: "20px", 
            width: '150px'}}>
          Choose what you'd like to measure fraud against:
          </div>
            <Grid col >
              {chartButtons.map((button) => (
                <Grid  id="chart-button-wrapper" key={button.id}>
                  <Button 
                    type='button'
                    name='Card Provider'
                    className={chartButton === button ? "active" : ""}
                    onClick={() => {
                      setChartButton(button.value)
                      setClicked(!clicked)
                      setDisplay(button.value)}} 
                    style={{
                      color: "#2E2C2F", 
                      marginLeft: '42px', 
                      padding: '25px', 
                      height: '100px', 
                      width: '150px' ,
                      marginBottom: '25px'}}
                    >{button.value}</Button>
              </Grid>
              ))}
            </Grid>
        </GridContainer>
    )
}

export default SelectionPanel