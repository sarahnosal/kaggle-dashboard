import React from 'react'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js'
import { Bar } from 'react-chartjs-2'

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
  );

ChartJS.defaults.font.family = "'DM Sans', sans-serif"

const BarChart2 = ({title, data}) => {
    const options = {
        scales: {
    
          x: {
            drawBorder: true,
            title: {
              display: true,
              text: 'Card Type',
              color: '#2E2C2F',
              font: {
                family: "'DM Sans', sans-serif",
                size: 14
              }
            },
            grid: {
              display: false,
              borderColor: "#2E2C2F"
            },
          },
          y: {
            drawBorder: true,
    
            suggestedMax: 10,
            title: {
              display: true,
              text: 'Fraud Rate(%)',
              font: {
                family: "'DM Sans', sans-serif",
                size: 14
              }
            },
            grid: {
              display: false,
              borderColor: "#2E2C2F",
            },
          },
        },
        plugins: {
          legend: {
            display: false,
            position: 'right',
            backgroundColor: '#D9D9D9',
          },
          title: {
            display: true,
            text: title,
            color: '#2E2C2F',
            font: {
              family: "'DM Sans', sans-serif",
              size: 20,
            }
          },
        },
      }

    const labels= ['Credit', 'Debit']

    const chartData = {
        labels,
        datasets: [
          {
            label: 'Fraud Rate (%)', 
            data: [data['credit'], data['debit']], 
            backgroundColor: ['#FCBFB7', '#A69888', '#729B79', '#334E58']},
        ]
      }

    return (
        <Bar style={{'backgroundColor':"#BACDB0", 'padding':'10px'}} options={options} className='bar' data={chartData}></Bar>
    )
}

export default BarChart2