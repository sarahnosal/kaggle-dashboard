import React from 'react'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js'
import { Bar } from 'react-chartjs-2'

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

ChartJS.defaults.font.family = "'DM Sans', sans-serif"

const BarChart1 = ({title, data}) => {

  const options = {
    scales: {

      x: {
        drawBorder: true,
        title: {
          display: true,
          text: 'Card Provider',
          color: '#2E2C2F',
          font: {
            family: "'DM Sans', sans-serif",
            size: 14
          }
        },
        grid: {
          display: false,
          borderColor: "#2E2C2F"
        },
      },
      y: {
        drawBorder: true,
        suggestedMax: 10,
        title: {
          display: true,
          text: 'Fraud Rate(%)',
          font: {
            family: "'DM Sans', sans-serif",
            size: 14
          }
        },
        grid: {
          display: false,
          borderColor: "#2E2C2F",
        },
      },
    },
    plugins: {
      legend: {
        display: false,
        position: 'right',
        backgroundColor: '#D9D9D9',
      },
      title: {
        display: true,
        text: title,
        color: '#2E2C2F',
        font: {
          family: "'DM Sans', sans-serif",
          size: 20,
        }
      },
    },
  }
  const labels= ['Discover', 'MasterCard', 'VISA', 'AmEx']

  const chartData = {
    labels,
    datasets: [
      {
        label: 'Fraud Rate (%)', 
        data: [data['discover'], data['mastercard'], data['visa'], data['american express']],
        backgroundColor: ['#FCBFB7', '#A69888', '#729B79', '#334E58']},
    ]
  }

  return (
    <Bar style={{'backgroundColor':"#BACDB0", 'padding':'10px'}} className='bar' options={options} data={chartData} />
  )
}

export default BarChart1