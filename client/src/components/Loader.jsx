import React from "react";
import styled, { css } from "styled-components";

export const Loader = styled.div(
    ({ width = 20, height = 20 }) => css`
    align-self: center;
    animation:spin 1s infinite linear;
    border:solid 2vmin transparent;
    border-radius:50%;
    border-right-color:#E6E6E6;
    border-top-color:#E6E6E6;
    box-sizing:border-box;
    height:${height}vmin;
    left:calc(50% - 10vmin);
    top:calc(50% - 10vmin);
    width:${width}vmin;
    z-index:1;
    &:before {
      animation:spin 2s infinite linear;
      border:solid 2vmin transparent;
      border-radius:50%;
      border-right-color:#808080;
      border-top-color:#808080;
      box-sizing:border-box;
      content:"";
      height:${height - 4}vmin;
      left:0;
      position:absolute;
      top:0;
      width:${width - 4}vmin;
    }
    &:after {
      animation:spin 3s infinite linear;
      border:solid 2vmin transparent;
      border-radius:50%;
      border-right-color:#000;
      border-top-color:#000;
      box-sizing:border-box;
      content:"";
      height:${height - 8}vmin;
      left:2vmin;
      position:absolute;
      top:2vmin;
      width:${width - 8}vmin;
    }
  }
  
  @keyframes spin {
    100% {
      transform:rotate(360deg);
    }
  }
  `
  );
  