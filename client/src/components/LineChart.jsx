import React from 'react'
import {
  Chart as ChartJS,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2'
import { Grid } from "@trussworks/react-uswds"

ChartJS.register(
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend,
);

const LineChart = ({title, dData}) => {

    const options = {
      responsive: true,
      plugins: {
        legend: {
          position: 'right',
          display: false,
          backgroundColor: '#D9D9D9',
        },
        title: {
          display: true,
          text: title,
          color: '#2E2C2F',
          font: {
            family: "'DM Sans', sans-serif",
            size: 20,
          }
        }
      },
      scales: {
        x: {
          drawBorder: true,
          beginAtZero: true,
          label: "Distance",
          title: {
            display: true,
            text: 'Distance*',
            color: '#2E2C2F',
            font: {
              family: "'DM Sans', sans-serif",
              size: 14
            }
          },
          grid: {
            display: false,
            borderColor: "#2E2C2F"
          },
        },
        y: {
          beginAtZero: true,
          drawBorder: true,
          suggestedMax: 10,
          label: "Fraud Rate (%)",
          title: {
            display: true,
            text: 'Fraud Rate(%)',
            font: {
              family: "'DM Sans', sans-serif",
              size: 14
            }
          },
          grid: {
            display: false,
            borderColor: "#2E2C2F",
          },
        }
      }
    }
    console.log(dData)
    // const format = delete dData['None']
    const dataset = Object.entries(dData)?.map(([key, value]) => ({
      x: key,
      y: value,
    }))

    console.log(dataset)
    const data = {
        datasets: [
          {
            label: {
              x: "Distance",
              y: "Fraud Rate (%)",
            },
            data: dataset,
            borderColor: "#334E58",
            backgroundColor: '#FCBFB7',
          }
        ]
      };

    return (
      <Grid >
        <Line style={{'backgroundColor':"#BACDB0", 'padding':'10px'}} data={data} options={options}></Line>
        <Grid style={{'backgroundColor':"#BACDB0", 'padding':'10px', 'textAlign': 'center', 'fontSize': '11px', 'color': '#2E2C2F'}}>
          Distance from IP address of each transaction to the billing address for the card used
        </Grid>
      </Grid>
    )
    
}

export default LineChart