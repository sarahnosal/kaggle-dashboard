module.exports = {
    extends: ["react-app"],
    rules: {
      "react/jsx-filename-extension": "off",
      "react/react-in-jsx-scope": "off",
      "no-unused-vars": "off",
      "no-use-before-define": "off",
      "no-param-reassign": "off",
      "no-return-assign": "off",
      "no-shadow": "off",
      "react/jsx-props-no-spreading": "off",
      "import/prefer-default-export": "off",
      "react/prop-types": 0,
      "no-console": "off",
      "react/no-array-index-key": "off",
      "jsx-a11y/label-has-associated-control": "off",
      "jsx-a11y/control-has-associated-label": "off",
    },
  };
  