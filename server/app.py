import pandas as pd
from fastapi import FastAPI
from sqlalchemy import create_engine
from api import router as api_router
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy import text
from db import config

app = FastAPI()
# app.config.from_mapping({
#     "DEBUG": True,
#     "CACHE_TYPE": "SimpleCache",
#     "CACHE_DEFAULT_TIMEOUT": 300
# })
# cache = Cache(app)
CORS_SETTINGS = config.settings.CORS_SETTINGS
print(CORS_SETTINGS)
origins = CORS_SETTINGS.split(',')
print(origins)

# TABLE_NAME = "train_transaction"
# DATABASE_URL = "postgresql://postgres:postgres@localhost:5432/postgres"
# engine = create_engine(DATABASE_URL)

# conn = engine.connect()    

# @app.post("/api/load-data")
# def load_data():
#         # Load data from file, only keep some columns
#         data = pd.read_csv(r'C:\Users\SarahNosal\Code\kaggle-dashboard\server\data\train_transaction.csv', encoding='utf8')[[
#             "TransactionID",
#             "isFraud",
#             "TransactionDT",
#             "TransactionAmt",
#             "ProductCD",
#             "card1",
#             "card2",
#             "card3",
#             "card4",
#             "card5",
#             "card6",
#             "addr1",
#             "addr2",
#             "dist1",
#             "dist2",
#             "P_emaildomain",
#             "R_emaildomain"
#         ]]

#         # Create bin columns
#         data["TransactionAmtBin"] = pd.cut(data["TransactionAmt"], [0, 20, 50, 100, 200, 500, 1000, 10000000],
#                                            labels=["0-20", "20-50", "50-100", "100-200", "200-500", "500-1000",
#                                                    "1000+"])
#         data["Dist1Bin"] = pd.cut(data["dist1"], [0, 1000, 2000, 5000, 10000000000],
#                           labels=["0-1000 mi", "1000-2000 mi", "2000-5000 mi", "5000+"])
#         data["Dist2Bin"] = pd.cut(data["dist2"], [0, 1000, 2000, 5000, 10000000000],
#                           labels=["0-1000 mi", "1000-2000 mi", "2000-5000 mi", "5000+"])

#         # Send data to db
#         conn.execute(text("DROP TABLE IF EXISTS " + TABLE_NAME))
#         data.to_sql(TABLE_NAME, engine, index=False, if_exists='replace')
#         return "Success", 200

# @app.get("/api/aggregate-data")
# # @cache.cached()
# def aggregate_data():
    
#     rv = {}
#     for col in ["TransactionAmtBin", "card4", "card6", "P_emaildomain", "Dist1Bin", "Dist2Bin"]:
#         rv[col] = {}
#         sql = 'SELECT "' + col + '", COUNT(*) AS total_count, SUM("isFraud") FROM ' + TABLE_NAME + ' GROUP BY "' + col + '" ORDER BY total_count DESC LIMIT 7'
#         for row in conn.execute(text(sql)):
#             unique_value = str(row[0])
#             total_count = int(row[1])
#             fraud_count = int(row[2])
#             rv[col][unique_value] = round((fraud_count / total_count)*100, 2)

#     return jsonify(rv)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    api_router,
)