from fastapi import APIRouter, Depends
from fastapi.encoders import jsonable_encoder
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from db.config import settings
from db.database import SessionLocal, engine

API = settings.API_V1_STR
USER = settings.POSTGRES_USER
PASSWORD = settings.POSTGRES_PASSWORD

router = APIRouter()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.get(f"{API}/", status_code=200)
async def root(db: Session = Depends(get_db)):
    print("Hello World")
    return print("Hello World")

@router.get(f"{API}/aggregate-data", status_code=200)
def aggregate_data(db: Session = Depends(get_db)):
    
    data = {}
    for col in ["TransactionAmt", "card4", "card6", "P_emaildomain", "dist1", "dist2"]:
        data[col] = {}
        sql = 'SELECT "' + col + '", COUNT(*) AS total_count, SUM("isFraud") FROM public.train_transaction GROUP BY "' + col + '" ORDER BY total_count DESC LIMIT 20'
        for row in jsonable_encoder(db.execute(sql).all()):
            unique_value = str(row[list(row.keys())[0]])
            total_count = int(row[list(row.keys())[1]])
            fraud_count = int(row[list(row.keys())[2]])
            data[col][unique_value] = round((fraud_count / total_count)*100, 2)
    print(data)
    return data
