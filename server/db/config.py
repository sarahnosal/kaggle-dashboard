import os
import secrets
from typing import Optional, Dict, Any
from pydantic import BaseSettings, PostgresDsn, validator

class Settings(BaseSettings):
    PROJECT_NAME: str = "kaggle_dashboard"
    API_V1_STR: str = "/api/v1"

    # CORS
    CORS_SETTINGS: str

    # POSTGRES CONFIG
    POSTGRES_SERVER: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    POSTGRES_PORT: str
    USE_SYBASE: Optional[str]
    SYBASE_DSN: Optional[str]
    SQLALCHEMY_PG_DATABASE_URI: Optional[PostgresDsn] = None

    @validator("SQLALCHEMY_PG_DATABASE_URI", pre=True, allow_reuse=True)
    def assemble_pg_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            port=values.get("POSTGRES_PORT"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    print(os.environ.get("ENVFILE"))

    class Config:
        case_sensitive = True
        env_file = os.environ.get("ENVFILE") or ".env"
        env_file_encoding = "utf-8"


settings = Settings()

print(Settings())
