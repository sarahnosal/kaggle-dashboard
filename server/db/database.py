from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from db.config import settings


print(settings.SQLALCHEMY_PG_DATABASE_URI)
engine = create_engine(
    settings.SQLALCHEMY_PG_DATABASE_URI, echo=False, pool_pre_ping=True, pool_size=100, max_overflow=50, pool_timeout=300 
)
Base = declarative_base()

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# data = pd.read_csv(r'C:\Users\SarahNosal\Code\kaggle-dashboard\server\data\train_transaction.csv', encoding='utf8')[[
#     "TransactionID",
#     "isFraud",
#     "TransactionDT",
#     "TransactionAmt",
#     "ProductCD",
#     "card1",
#     "card2",
#     "card3",
#     "card4",
#     "card5",
#     "card6",
#     "addr1",
#     "addr2",
#     "dist1",
#     "dist2",
#     "P_emaildomain",
#     "R_emaildomain"
# ]]

# TABLE_NAME = "jtw_train_transaction"
# DATABASE_URL = "postgresql://asr:asr-kaggle-training@training:5432/asr"
# engine = create_engine(DATABASE_URL)
# data.to_sql(TABLE_NAME, engine, index=False)